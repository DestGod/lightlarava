<?php

$router = new Router(new Request);

$router->get('/', function ($request) {
    return var_dump($request);
});

$router->get('/users', function ($request) {
    return 'users list';
});

$router->get('/users/1', function ($request) {
    return 'user N 1';
});