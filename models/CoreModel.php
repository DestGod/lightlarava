<?php

namespace models;

abstract class CoreModel {

    protected $table;

    protected $db_connection;

    public function __construct() {
        $this->db_connection = new \PDO('mysql:host=' . $GLOBALS['DB_HOST'] . ';dbname=' . $GLOBALS['DB_DATABASE'], $GLOBALS['DB_USERNAME'], $GLOBALS['DB_PASSWORD']);

        foreach ($this->getModelColumns() as $column) {
            $this->{$column} = null;
        }
    }

    final protected function getConnection(): \PDO {
        return $this->db_connection;
    }

    final public function getTableName(): string {
        if ($this->table) {
            return $this->table;
        } else {
            $splitted_path = explode('\\', get_class($this));

            return strtolower($splitted_path[count($splitted_path) - 1]) . 's';
        }
    }

    final public function getModelColumns(): array {
        $q = $this->getConnection()->prepare("DESCRIBE {$this->getTableName()}");
        $q->execute();
        return $q->fetchAll(\PDO::FETCH_COLUMN);
    }


    public function find(int $id = null) {
        if (isset($id)) {
            $query = "SELECT * FROM `" . $this->getTableName() . "` WHERE id = :id";
            $params = [
                ':id' => $id
            ];

            $stmt = $this->getConnection()->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

    public function fill(array $data = []): bool {
        $query = "INSERT INTO `" . $this->getTableName() . "` (";

        foreach ($data as $key => $value) {
            $query .= "`" . $key . "`,";
        }

        $query = substr($query, 0, -1);
        $query .= ") VALUES (";

        $params = [];

        foreach ($data as $key => $value) {
            $params[':' . $key] = $value;
        }

        foreach ($params as $key => $value) {
            $query .= $key . ',';
        }

        $query = substr($query, 0, -1);
        $query .= ')';


        $stmt = $this->getConnection()->prepare($query);
        return $stmt->execute($params);
    }

    public function destroy(int $id = null) {
        if ($model = $this->find($id)) {
            $query = "DELETE FROM `" . $this->getTableName() . "` WHERE id = :id";
            $params = [
                ':id' => $id
            ];

            $stmt = $this->getConnection()->prepare($query);
            return $stmt->execute($params);
        } else {
            return false;
        }
    }

    public function all() {
        $query = "SELECT * FROM `{$this->getTableName()}`";
        $stmt = $this->getConnection()->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

}