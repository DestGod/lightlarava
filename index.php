<?php

require_once 'config.php';

require_once 'routes/Request.php';
require_once 'routes/Router.php';

$router = new Router(new Request);

$router->get('/', function () {
    return 'Light Larava';
});

$router->get('/users', function ($request) {

    require_once 'models/User.php';

    $user = new \models\User();

    return json_encode(get_object_vars($user));
});
